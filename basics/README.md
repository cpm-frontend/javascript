# JavaScript Basics

## DOM Manipulation
- When a web page is loaded, the browser creates a Document Object Model (**DOM**) of the page. It defines:
    1. The HTML elements as objects
    1. The properties of all HTML elements
    1. The methods to access all HTML elements
    1. The events for all HTML elements
- With the DOM, JavaScript can access and change all the elements of an HTML document.

### Target Nodes
- A **node** is any object in the DOM hierarchy while an **element** is one specific node (there are many other types of nodes such as text nodes, comment nodes, document nodes, etc...).
- For the following example, `display` is a **child node** of a `container` and a **sibling node** of `control`.
    ```html
    <div id="container">
        <div class="display"></div>
        <div class="controls"></div>
    </div>
    ```
- For CSS, we have several ways to target `display`.
    - `div.display`
    - `.display`
    - `#container > .display`
    - `div#container > div.display`
- For DOM, we can target `display` as follows.
    ```js
    const container = document.querySelector('#container');
    // select the #container div 

    console.dir(container.firstElementChild);                      
    // select the first child of #container
    ```
- The **document object** is the root node of the HTML document. Here are some methods of document object to find HTML element(s):
    - `querySelector`: returns the first element that matches a specified CSS selector(s) in the document. If no match is found, it returns null.
    - `querySelectorAll`: returns a static NodeList object(a list of nodes) containing all elements that matches a specified CSS selector(s) in the document.
    - `getElementById`: get the element with the specified ID.
    - `getElementsByClassName`: get all elements with the specified class name.
    - `getElementsByName`: get all elements with the specified name.
    - `getElementsByTagName`: get all elements in the document with the specified tag name.
- Here are some methods of **Element object** to find HTML element(s):
    - `firstElementChild`: returns the first child element of the specified element.
    - `querySelector`: returns the first child element that matches a specified CSS selector(s) of an element.
    - `querySelectorAll`: returns all child elements that matches a specified CSS selector(s) of an element.

### Creating Elements
- We can create a new element `button` as follows. Notice that this function does ***NOT*** put your new element into the DOM, it just simply creates it in memory.
    ```js
    const btn = document.createElement('BUTTON');
    ```
- After creating, you can manipulate the element before placing it on the page. For example, we are going to add a text on this button. Finally we put it into our DOM.
    ```js
    const btn = document.createElement('BUTTON');
    btn.innerHTML = "CLICK ME";
    document.body.appendChild(btn);
    ```
- Here are some methods of **Element object** to append element(s).
    - `appendChild`: appends a node as the last child of a node.
    - `insertBefore`: inserts a new child node before a specified or existing child node.
        ```js
        var newItem = document.createElement("LI");
        // Create a <li> tag node

        var textnode = document.createTextNode("Water");
        // Create a text node

        newItem.appendChild(textnode);

        var list = document.getElementById("#myList");
        // Get the <ul> element which ID is "myList" to insert a new node

        list.insertBefore(newItem, list.childNodes[0]);  // Insert <li> before the first child of <ul>
        ```
### Removing Elements
For the folloing example, we can click the button **Try it** to trigger the function `myFunction` to remove the first child of `<ul>` element. If you click once, **Coffee** will be removed. If you click twice, **Tea** will be removed.
```html
<body>
    <ul id="myList">
        <li>Coffee</li>
        <li>Tea</li>
        <li>Milk</li>
    </ul>
    <button onclick="myFunction()">Try it</button>

    <script>
        function myFunction() {
            var list = document.getElementById("myList");
            list.removeChild(list.childNodes[0]);
        }
    </script>
</body>
```
### Replacing Elements
In this example, we can click the button **Try it** to trigger the function `myFunction` to replace **Coffee** by **Water**.
```html
<body>
    <ul id="myList">
        <li>Coffee</li>
        <li>Tea</li>
        <li>Milk</li>
    </ul>
    <button onclick="myFunction()">Try it</button>
    <script>
        function myFunction() {
            var textnode = document.createTextNode("Water");
            var item = document.getElementById("myList").childNodes[0];
            item.replaceChild(textnode, item.childNodes[0]);
        }
    </script>
</body>
```
### Altering Elements
When you have a reference to an element, you can use that reference to alter the properties.
- For example, you can add inline CSS style as follows.
    ```js
    const div = document.createElement('div');                     
    div.style.color = 'blue';                                      
    // adds the indicated style rule

    div.style.cssText = 'color: blue; background: white';          
    // adds several style rules
    ```
- Another common method to alter property is to use `setAttribute`.
    ```js
    div.setAttribute('style', 'color: blue; background: white');
    ```
- Notice that you are accessing a CSS rule from JS, you'll either need to use camel case or you'll need to use bracket notation instead of dot notation.
    ```js
    div.style.background-color // doesn't work
    div.style.backgroundColor // accesses the divs background-color style
    div.style['background-color'] // also works
    div.style.cssText = "background-color: white" // ok in a string
    ```
- The `setAttribute` method either adds a new attribute to an HTML element, or **updates** the value of an attribute that already exists.
    ```html
    <input value="OK">
    <button onclick="myFunction()">Try it</button>
    <script>
        function myFunction() {
            document.getElementsByTagName("INPUT")[0].setAttribute("value", "ALTERED"); 
        }
    </script>
    ```
- We can use following code to get attribute and remove attribute.
    ```js
    div.setAttribute('id', 'theDiv');                              
    div.getAttribute('id');     // "theDiv"
    div.removeAttribute('id');  // removes specified attribute
    ```
- If we want to control the `class` property, we are used to call `classList` to get the class name(s) of an element. If you have not given an element any class, it will return empty array. The following methods can control the class.
    - `add`: adds one or more class names to an element.
        ```js
        div.classList.add('class1', 'class2');
        ```
    - `remove`: removes one or more class names from an element.
        ```js
        div.classList.remove('class1', 'class2');
        ```
    - `toggle`: toggles between a class name for an element.
        ```js
        div.classList.toggle('active'); 
        // if div doesn't have class "active" then add it, or remove it
        ```
- Add text content by `textContent`. The following example creates a text node containing "Hello World!" and inserts it in `div`.
    ```js
    div.textContent = 'Hello World!'
    ```
- You can also use `innerHTML` to render the HTML inside `div` directly. Note that `textContent` is preferable for adding text, and `innerHTML` should be used sparingly as it can create security risks if misused.
    ```js
    div.innerHTML = '<span>Hello World!</span>'; 
    ```
### Events
- A JavaScript can be executed when an event occurs, like when a user clicks on an HTML element. There are three primary ways to go about this:
    1. Attach functions attributes directly on your HTML elements. This solution is less than ideal because we're cluttering our HTML with JavaScript.
        ```html
        <button onclick="alert('Hello World')">Click Me</button>
        ```
    1. Set the event property (name of this property always is `on + [event]`, e.g. `onClick`) on the DOM object in your JavaScript. 
        ```html
        <!-- the HTML file -->
        <button id="btn">Click Me</button>
        ```
        ```js
        // the JavaScript file
        const btn = document.querySelector('#btn');
        btn.onclick = () => alert("Hello World");
        ```
    1. Attach event listeners to the nodes in your JavaScript. This solution allows multiple event listeners if the need arises. 
        ```html
        <!-- the HTML file -->
        <button id="btn">Click Me Too</button>
        ```
        ```js
        // the JavaScript file
        const btn = document.querySelector('#btn');
            btn.addEventListener('click', () => {
            alert("Hello World");
        });
        ```
- Note that the above 3 methods can be used with named functions. Using named functions can clean up your code considerably and you can reuse it.
    - Here is our function.
        ```js
        function alertFunction() {
            alert("Hello World");
        }
        ```
    - 3 methods can trigger above function.
        ```html
        <!-- the html file -->
        <!-- METHOD 1 -->
        <button onclick="alertFunction()">CLICK ME</button>
        ```
        ```js
        // METHOD 2
        btn.onclick = alertFunction;

        // METHOD 3
        btn.addEventListener('click', alertFunction);
        ```
- We can access more information about the event by passing a parameter to the function that we are calling. The `e` in that function is an object that references the event itself. 
    ```js
    btn.addEventListener('click', function (e) {
        console.log(e);
    });
    ```
- We can access DOM node that was clicked by `e.target`.
    ```js
    btn.addEventListener('click', function (e) {
        e.target.style.background = 'blue';
    });
    ```
- If we don't need an event anymore, we can detach it.
    ```js
    btn.removeEventListener('click', alertFunction);
    ```

## Fetch API

- The Fetch API provides a `fetch()` method defined on the `window` object (and `WorkerGlobalScope` object), which you can use to perform requests. This method returns a `Promise` that you can use to retrieve the response of the request. 
- `fetch()` is a biult-in method in browser, but not for **NodeJS**. If you want to use it in **NodeJS** environment, we should import `node-fetch`.
- In the following examples, you can run them on **React** environment.
    1. Go to [fetch-api/api/](fetch-api/api/) directory.
        ```
        cd fetch-api/api
        ```
    1. Install the dependencies.
        ```
        yarn
        ```
    1. Run the script to start the JSON server.
        ```
        yarn start
        ```
    1. Open another terminal and go to [fetch-api/client/](fetch-api/client) folder.
        ```
        cd fetch/client
        ```
    1. Install the dependencies.
        ```
        yarn
        ```
    1. Run the script to start **React** application.
        ```
        yarn start
        ```
    1. Visit [http://localhost:3000](http://localhost:3000).

### GET Request
- You can pass the URL which is the local resource you want to fetch. In **React**, the local resource files are under [public/](fetch-api/public/) folder. 
    ```js
    fetch('/users.json')
        .then(response => response.text())
        .then(data => console.log(data))
    ```
- There are many methods of `response` you can use.
    - `clone()`: As the method implies this method creates a clone of the response.
    - `redirect(url)`: This method creates a new response but with a different URL.
    - `arrayBuffer()`: returns a promise that resolves with an `ArrayBuffer`.
    - `formData()`: returns a promise but one that resolves with `FormData` object.
    - `blob()`: is one resolves with a `Blob`.
    - `text()`: resolves with a string.
    - `json()`: resolves the promise with JSON.
- You can use `async/await` to use `fetch`. 
    ```js
    async function fetch() {
        const response = await fetch('/users.json');
        const data = await response.text();
        console.log(data);
    }
    ```
- We can get the response status from `Response` object.
    ```js
    const response = await fetch('/users.json');

    console.log(response.status); // 200
    console.log(response.statusText); // OK

    if (response.status === 200) {
        const data = await response.text();
        // handle data
    }
    ```
- You can pass the HTTP request to `fetch` for calling API.
    ```js
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.text())
        .then(data => console.log(data))
    ```
- If you want to add headers or other parameters for your request, there are 2 ways to do.
    - Using `Request`.
        ```js
        const request = new Request('https://jsonplaceholder.typicode.com/users', {
            method: 'GET',
            headers: new Headers({
                'Content-Type': 'text/plain'
            })
        });
        fetch(request)
            .then(response => response.text())
            .then(data => console.log(data));
        ```
    - Add additional parameter in `fetch()`.
        ```js
        fetch('https://jsonplaceholder.typicode.com/users', {
            method: 'GET', 
            headers: new Headers({
                'Content-Type': 'text/plain'
            })
        }).then(response => response.text())
            .then(data => console.log(data));
        ```
### Other Requests (POST, PUT and so on)
- For POST method, we can either set up in `Request` or add additional parameter in `fetch` like above example.
    - Using `Request`.
        ```js
        const user = {
            name: 'John',
            surname: 'Smith'
        };
        const request = new Request('/users.json', {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json;charset=utf-8'
            }),
            body: JSON.stringify(user)
        });
        const result = await response.json();
        ```
    - Add additional parameter in `fetch()`. Notice that you can use object directly for `headers` property, you don't have to use `Headers` object (`Headers` is also fine).
        ```js
        const user = {
            name: 'John',
            surname: 'Smith'
        };

        const response = await fetch('/users.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(user)
        });
        const result = await response.json();
        ```
- For other request methods, please try youself.

## AJAX
## ES6+

## Common Concepts

### Hoisting
Hoisting is JavaScript's default behavior of moving all declarations to the top of the current scope (to the top of the current script or the current function).Notice that variables and constants declared with `let` or `const` are ***NOT*** hoisted! For the following examples, you can go to [hoisting/](hoisting/) folder to try youself.
- In JavaScript, a variable can be declared after it has been used.
    ```js
    x = 5;              // Assign 5 to x
    console.log(x);     // 5
    var x;              // Declare x
    ```
- JavaScript only hoists declarations, not initializations.
    ```js
    var x = 5;                  // Initialize x
    console.log(x + " " + y);   // 5 undefined
    var y = 7;                  // Initialize y
    ```
- Same result with above example.
    ```js
    var x = 5;                  // Initialize x
    var y;                      // Declare y
    console.log(x + " " + y);   // 5 undefined
    y = 7;                      // Initialize y
    ```
- In fact, we should always declare all variables at the beginning of every scope to avoid bugs. JavaScript in **strict mode** does ***NOT*** allow variables to be used if they are not declared (**strict mode** is a built-in setting in **NodeJS**).

### Strict Mode
**Strict mode** is declared by adding `"use strict"` to the beginning of a script or a function. If you declare at the beginning of a script, it has global scope (all code in the script will execute in strict mode). Notice that the `"use strict"` directive is ***ONLY*** recognized at the beginning of a script or a function.
- **Strict mode** makes it easier to write secure JavaScript. For example, you can not use undeclared variables or object.
    ```js
    "use strict";
    x = 3.14;                // This will cause an error
    ```
- Deleting a variable, an object or a function is not allowed.
    ```js
    "use strict";
    var x = 3.14;
    delete x;                // This will cause an error
    ```
- Duplicating a parameter name of function is not allowed:
    ```js
    "use strict";
    function x(p1, p1) {};   // This will cause an error
    ```
- Keywords reserved for JavaScript can ***NOT*** be used as variable names in **strict mode**. These are: 
    - implements
    - interface
    - let
    - package
    - private
    - protected
    - public
    - static
    - yield
    - eval
    - arguments
- The `this` keyword refers to the object that called the function. If the object is not specified, functions in **strict mode** will return `undefined` and functions in normal mode will return the global object (window):
    ```js
    "use strict";
    function myFunction() {
        alert(this);    // will alert "undefined"
    }
    myFunction();
    ```

### Event Bubbling
When an event happens on an element, it first runs the handlers on it, then on its parent, then all the way up on other ancestors. 
- Let's say we have 3 nested elements `form`, `div`, and `p` with a handler on each of them:
    ```html
    <style>
        body * {
            margin: 10px;
            border: 1px solid blue;
        }
    </style>
    <form onclick="alert('form')">FORM
        <div onclick="alert('div')">DIV
            <p onclick="alert('p')">P</p>
        </div>
    </form>
    ```
- So if we click on `<p>`, then we'll see 3 alerts as this order: p, div, form. The process is called **bubbling**, because events bubble from the inner element up through parents like a bubble in the water.
- Notice that ***ALMOST*** all events bubble. For instance, a `focus` event does not bubble.

### Scope
Scope determines the accessibility (visibility) of variables. In JavaScript there are two types of scope: **Local scope** and **Global scope**.
- Local JavaScript variables declared within a JavaScript function. Local variables have **function scope** that can only be accessed from within the function.
    ```js
    // code here can NOT use car
    function myFunction() {
        var car = "Volvo";
        // code here CAN use car
    }
    ```
- Global JavaScript variables declared outside a function. A global variable has **global scope** that all scripts and functions on a web page can access it. 
    ```js
    var car = "Volvo";
    // code here can use car

    function myFunction() {
        // code here can also use car
    }
    ```
- If you assign a value to a variable that has not been declared, it will automatically become a **global** variable. But all modern browsers support running JavaScript in `"Strict Mode"`. In strict mode, undeclared variables are not automatically global.
    ```js
    myFunction();
    // code here CAN use car

    function myFunction() {
        car = "Volvo";
    }
    ```
- In HTML, the global scope is the window object. All global variables belong to the window object.
    ```js
    var car = "Volvo";
    // code here can use window.car
    ```
- Lifetime of variables
    - The lifetime of a JavaScript variable starts when it is declared.
    - Local variables are deleted when the function is completed.
    - In a web browser, global variables are deleted when you close the browser window (or tab).

### Prototype
- JavaScript is a dynamic language. You can attach new properties to an object at any time as shown below.
    ```js
    function Student(name, gender) {
        this.name = name;
        this.gender = gender;
    }
    var student1 = new Student('John', 'Male');
    student1.language = 'en';
    console.log(student1.language);
    var student2 = new Student('Marry', 'Female');
    console.log(student2.language);  
    ```
- However, `student2` instance will not have `language` property because it is defined only on `student1` instance. In other word, you can ***NOT*** add a new property to an existing object constructor like so.
    ```js
    Student.language = 'en';    // you cannot do it
    ```
- All JavaScript objects inherit properties and methods from a prototype like:
    - `Date` objects inherit from `Date.prototype`
    - `Array` objects inherit from `Array.prototype`
    - `Student` objects inherit from `Student.prototype`
- The `Object.prototype` is on the top of the prototype inheritance chain. `Date` objects, `Array` objects, and `Student` objects inherit from `Object.prototype`.
- Using the `prototype` property, you can add new properties (or methods) to an object constructor and all existing objects of a given type.
    ```js
    function Student(name, gender) {
        this.name = name;
        this.gender = gender;
    }
    var student1 = new Student('John', 'Male');
    var student2 = new Student('Marry', 'Female');
    Student.prototype.language = 'en';
    Student.prototype.info = function() {
        return this.name + ": " + this.gender;
    };
    console.log(student1.language); // en
    console.log(student2.language); // en
    console.log(student1.info());   // John: Male
    console.log(student2.info());   // Marry: Female
    ```
- Every object which is created using literal syntax or constructor syntax with the new keyword, includes `__proto__` property that points to prototype object of a function that created this object.
    ```js
    var student = new Student('John', 'Male');
    console.log(Student.prototype); // object
    console.log(student.prototype); // undefined
    console.log(student.__proto__); // object

    console.log(typeof Student.prototype);  // object
    console.log(typeof student.__proto__);  // object

    console.log(Student.prototype === student.__proto__ );  // true
    ```
- In above example, you can find the instance (`student`) does not expose `prototype` property. If you want to access `prototype` object of instance, you have to use `Object.getPrototypeOf(obj)`.
    ```js
    var proto = Object.getPrototypeOf(student);  // returns Student's prototype object
            
    console.log(proto.constructor); // returns Student function 
    ```
- The `prototype` object includes following properties and methods.
    Property|Description
    --|--
    `constructor`|Returns a function that created instance.
    `__proto__`|This is invisible property of an object. It returns prototype object of a function to which it links to.

    Method|Description
    --|--
    `hasOwnProperty()`|Returns a boolean indicating whether an object contains the specified property as a direct property of that object and not inherited through the prototype chain.
    `isPrototypeOf()`|Returns a boolean indication whether the specified object is in the prototype chain of the object this method is called upon.
    `propertyIsEnumerable()`|Returns a boolean that indicates whether the specified property is enumerable or not.
    `toLocaleString()`|Returns string in local format.
    `toString()`|Returns string.
    `valueOf`|Returns the primitive value of the specified object.
- There are 2 ways to change prototype. Notice that **if you change function's prototype then ***ONLY*** new objects will be linked to changed prototype. All other existing objects will still link to old prototype of function**. 
    ```js
    function Student() {
        this.name = 'John';
        this.gender = 'M';
    }
    var student1 = new Student();
    Student.prototype.age = 15;         // method 1

    var student2 = new Student();
    Student.prototype = { age : 20 };   // method 2

    var student3 = new Student();
    console.log(student1.age);  // 15
    console.log(student2.age);  // 15
    console.log(student3.age);  // 20
    ```
- The prototype object is being used by JavaScript engine in 2 things. One is to find properties and methods of an object, another is to implement inheritance in JavaScript (e.g. `toString()`). In the following example, JavaScript engine checks whether `toString()` method is attached to `student`. If it does not find there then it uses `student.__proto__` link which points to the `prototype` object of `Student` function. If it still cannot find it there then it goes up in the heirarchy and check `prototype` object of `Object` function.
    ```js
    function Student() {
        this.name = 'John';
        this.gender = 'M';
    }
    var student = new Student();
    student.toString();
    ```
### Shadow DOM
- All elements and styles within an HTML document, and therefore the DOM, are in one big global scope. Any element on the page can be accessed by the `document.querySelector()`. But sometimes we don't want it to be affected by even global styles. The solution is **Shadow DOM**.
- The Shadow DOM allows a component author to create a sub-DOM tree, and it allows hidden DOM trees to be attached to elements in the regular DOM tree. This shadow DOM tree starts with a shadow root, underneath which can be attached to any elements you want, in the same way as the normal DOM. Look at following picture, there are some bits of shadow DOM terminology to be aware of:
    - **Shadow host**: the regular DOM node that the shadow DOM is attached to.
    - **Shadow tree**: the DOM tree inside the shadow DOM.
    - **Shadow boundary**: the place where the shadow DOM ends, and the regular DOM begins.
    - **Shadow root**: the root node of the shadow tree.
    <img src="screenshots/shadow-dom.png" alt="shadow-dom" />
- You can affect the nodes in the shadow DOM in exactly the same way as non-shadow nodes. For example appending children or setting attributes, styling individual nodes or adding style to the entire shadow DOM tree inside a `<style>` element. The difference is that none of the code inside a shadow DOM can affect anything outside it.
- You can attach a shadow root to any element using the `attachShadow()` which is element object method. Notice that when you attach shadow DOM to a specific element, you create a new DOM to attach on it, so it's ***EMPTY***. If you get the element from normal DOM as a shadow host, it's independent on shadow root which is new and empty. In other word, the elements or text in normal DOM under this shadow host, it will not show up (it's overridden by shadow DOM).
    ```js
    const shadowHost = document.querySelector(".shadow-host");
    const shadow = shadowHost.attachShadow({mode: 'open'});
    console.log(shadowHost.shadowRoot.innerHTML); // empty string 
    ```
- There are 2 mode of `attachShadow()`.
    - `mode: 'open'` means that you can access the shadow DOM using JavaScript written in the main page context. You can use `shadowRoot` property to get this shadow root.
        ```js
        shadowHost.attachShadow({mode: 'open'});
        shadowHost.shadowRoot.innerHTML = "You can access from the outside.";
        ```
    - `mode: 'closed'` means you won't be able to access the shadow DOM from the outside. Of course, you can access from the inside.
        ```js
        const shadow = shadowHost.attachShadow({mode: 'closed'});
        console.log(shadowHost.shadowRoot); // null
        shadow.innerHTML = "You can only access from the inside.";
        ```
- You can add more element and style for shadow DOM, please see [shadow-dom-1.html](shadow-dom/shadow-dom-1.html). The reference of this example is [here](https://bitsofco.de/what-is-the-shadow-dom/).
- You can also create shadow DOM and attach it to your custom element, then put your custom element in normal DOM, please see example [shadow-dom-2.html](shadow-dom/shadow-dom-2.html). The reference of this example is [here](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM).

## Linters and Formatters

**Lint**, or a **Linter**, is a tool that analyzes source code to flag programming errors, bugs, stylistic errors, and suspicious constructs (according to [wiki](https://en.wikipedia.org/wiki/Lint_(software)) definition). **Formatter** can make you code use same format everywhere.

### ESLint

- ESLint analyzes your code for style and coding errors that can lead to bugs. Now we create the new project first.
    ```
    yarn init
    ```
- You can use following command to install ESLint in your project.
    ```
    yarn add eslint --dev
    ```
- Then initialize ESlint in the project after installing.
    ```
    npx eslint --init
    ```
- After inputting above command, you need to answer following questions:
    - **How would you like to use ESLint?** - Choose last option to cover all.
    - **What type of modules does your project use?** - First option **JavaScript module (import/export)** is suitable if bebal is installed in your project (e.g. React, Vue and Angular). The second option is **CommonJS** which is good for **NodeJS** projects.
    - **Which framework does your project use?** - React, Vue, or other.
    - **Does your project use TypeScript?** - Yes or no.
    - **Where does your code run?** - Browser or Node.
    - **How would you like to define a style for your project?** - You can go with the default **Use a popular style guide**.
    - **Which style guide do you want to follow?** - Google, Alibaba or standard.
    - **What format do you want your config file to be in?** - Choose JSON.
    - **Would you like to install them now with npm?** - Choose yes. If you want to add it by yourself, you should install `eslint-config-google` like so.
- You can see the new file [.eslintrc.json](linter-formatter/eslint/.eslintrc.json) is created. This file defines your configuration of linter. You can change it or add new rule in rules section in this file. For the custom rules, you can check official page [here](https://eslint.org/docs/rules/).
    ```json
    {
        "env": {
            "browser": true,
            "commonjs": true,
            "es2020": true
        },
        "extends": [
            "google"
        ],
        "parserOptions": {
            "ecmaVersion": 11
        },
        "rules": {
            "semi": ["error", "always"],
            "quotes": ["error", "double"]
        }
    }
    ```
- Add script to your [package.json](linter-formatter/eslint/package.json). If you want to check all JS files under `src`, you can use set it up like so.
    ```json
    {
        ...
        "devDependencies": {
            "eslint": "^7.4.0"
        },
        "scripts": {
            "lint": "eslint src/*.js"
        }
    }
    ```
- Run `yarn lint` then you can trigger the lint.
- Sometimes we want run `yarn lint` before committing the code. Then you should take following steps.
    1. Install **Husky** to enable run commands or script before committing or pushing our code to git.
        ```
        yarn add husky --dev
        ```
    1. Set up in your [package.json](linter-formatter/eslint/package.json).
        ```json
        {
            ...
            "husky": {
                "hooks": {
                    "pre-commit": "yarn lint"
                }
            }
        }
        ```
- Now you can try to delete last line of [index.js](linter-formatter/eslint/src/index.js) and commit it. Linter will be triggered automatically and you will fail to commit.

### Prettier
- **Prettier** is an opinionated code formatter with support for JS, Angular, HTML and so on.
- What's the difference between **Prettier** and **ESLint**? **Linters** have two categories of rules, let's compare each of them to **Prettier**:
    1. **Formatting rules**: Prettier alleviates the need for this whole category of rules! Prettier is going to reprint the entire program from scratch in a consistent way, so it's not possible for the programmer to make a mistake there anymore.
    1. **Code-quality rules**: Prettier does nothing to help with those kind of rules.
- For summary, we should **use Prettier for formatting and linters for catching bugs**.
- Now we create the new project and install Prettier.
    ```
    yarn init
    yarn add --dev --exact prettier
    ```
- Create an empty file named `.prettierrc.json` under the root folder. You can check settings in official website [here](https://prettier.io/docs/en/options.html).
    ```json
    {
        "trailingComma": "es5",
        "tabWidth": 4,
        "semi": false,
        "singleQuote": true
    }
    ```
- Create a `.prettierignore` file to let the Prettier CLI and editors know which files to not format (like `.gitignore`).
    ```
    # Ignore artifacts:
    build
    coverage
    *.html
    ```
- Then you can run following command to format all files with Prettier. `.` means all, you can change to `app/` or `app/**/*.test.js` like so.
    ```
    yarn prettier --write .
    ```
- If you don't want Prettier fix format issue for you, you can use `check`.
    ```
    yarn prettier --check .
    ```
### ESlint & Prettier
- We should use Prettier for formatting and linters for catching bugs. But sometimes they might conflict with each other. We can use `eslint-config-prettier` package to avoid them. Please check out [eslint-with-prettier/](linter-formatter/eslint-with-prettier/).


