function Student(name, gender) {
    this.name = name;
    this.gender = gender;
}
var student1 = new Student('John', 'Male');
var student2 = new Student('Marry', 'Female');
Student.prototype.language = 'en';
Student.prototype.info = function() {
    return this.name + ": " + this.gender;
};
console.log(student1.language); // en
console.log(student2.language); // en
console.log(student1.info());   // John: Male
console.log(student2.info());   // Marry: Female
