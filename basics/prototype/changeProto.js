function Student() {
    this.name = 'John';
    this.gender = 'M';
}
var student1 = new Student();
Student.prototype.age = 15;         // method 1

var student2 = new Student();
Student.prototype = { age : 20 };   // method 2

var student3 = new Student();
console.log(student1.age);
console.log(student2.age);
console.log(student3.age);
