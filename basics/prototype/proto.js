function Student(name, gender) {
    this.name = name;
    this.gender = gender;
}

var student = new Student('John', 'Male');
console.log(Student.prototype); // object
console.log(student.prototype); // undefined
console.log(student.__proto__); // object

console.log(typeof Student.prototype);  // object
console.log(typeof student.__proto__);  // object

console.log(Student.prototype === student.__proto__ );  // true

// getPrototypeOf

var proto = Object.getPrototypeOf(student); // returns Student's prototype object
console.log(proto.constructor);             // returns Student function
