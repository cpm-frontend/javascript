import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {

    constructor() {
        super();
        this.state = {
            users: [],
            newUserName: "",
            newUserAge: "",
            idToDelete: ""
        };
    }

    componentDidMount() {
        this.fetchUsers();
    };

    handleInputChange = event => {
        const target = event.target;
        this.setState({
            [target.name]: target.value
        });
    };

    fetchUsers = async () => {
        const users = await fetch('http://localhost:3001/users')
            .then(response => response.text())
            .then(text => JSON.parse(text));
        this.setState({ users: users });
    }

    createUser = async () => {
        const user = {name: this.state.newUserName, age: this.state.newUserAge};
        const createdUser = await fetch('http://localhost:3001/users', {
            method: "POST",
            body: JSON.stringify(user),
            headers: {"Content-Type": "application/json"}
        });
        this.setState({users: {...this.users, createdUser}});
    }

    deleteUser = async () => {
        const id = this.state.idToDelete;
        await fetch(`http://localhost:3001/users/${id}`, {
            method: "DELETE",
            headers: {"Content-Type": "application/json"}
        });
    }

    render() {
        if(!this.state.users.length) {
            return <div>Loading...</div>;
        }
        return (
            <div>
                <div>
                    <h3>User List</h3>
                    <ul>
                        {this.state.users.map(user => {
                            return (
                                <li key={user.id}>
                                    {`${user.id}: ${user.name} is ${user.age} years old`}
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <div>
                    <h3>Create a User</h3>
                    <form onSubmit={this.createUser} className="ui form">
                        <div className="field">
                            <label>User Name: </label>
                            <input
                                type="text"
                                name="newUserName"
                                value={this.state.newUserName}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <div className="field">
                            <label>User Age: </label>
                            <input
                                type="number"
                                name="newUserAge"
                                value={this.state.newUserAge}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <input type="submit" name="submit" />
                    </form>
                </div>
                <div>
                    <h3>Delete a User</h3>
                    <form onSubmit={this.deleteUser} className="ui form">
                        <div className="field">
                            <label>User ID: </label>
                            <input
                                type="number"
                                name="idToDelete"
                                value={this.state.idToDelete}
                                onChange={this.handleInputChange}
                            />
                        </div>
                        <input type="submit" name="submit" />
                    </form>
                </div>
            </div>
        );
    }
};

ReactDOM.render(<App />, document.querySelector('#root'));
