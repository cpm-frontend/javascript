// Example 1

a = 5;              // Assign 5 to x
console.log(a);     // 5
var a;              // Declare x

// Example 2

var b = 5;                  // Initialize x
console.log(b + " " + c);   // 5 undefined
var c = 7;                  // Initialize y

// Example 3

var d = 5;                  // Initialize x
var e;                      // Declare y
console.log(d + " " + e);   // 5 undefined
e = 7;                      // Initialize y
