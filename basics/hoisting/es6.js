// ES6 let is not hoisted

try {
    a = 5;
    console.log(a);
    let a;
} catch (e) {
    console.log(e);
}

// ES6 const is not hoisted

try {
    console.log(b);
    const b = 7;
} catch (e) {
    console.log(e);
}